opsu-tmbl-master-py3
Salt install method: git
# salt-call --versions-report
Salt Version:
          Salt: 3007.0+0na.0a98359
 
Python Version:
        Python: 3.11.4 (main, Jun 28 2023, 19:51:46) [GCC]
 
Dependency Versions:
          cffi: 1.15.1
      cherrypy: Not Installed
      dateutil: Not Installed
     docker-py: Not Installed
         gitdb: Not Installed
     gitpython: Not Installed
        Jinja2: 3.1.2
       libgit2: Not Installed
  looseversion: 1.3.0
      M2Crypto: Not Installed
          Mako: Not Installed
       msgpack: 1.0.5
  msgpack-pure: Not Installed
  mysql-python: Not Installed
     packaging: 23.1
     pycparser: 2.21
      pycrypto: Not Installed
  pycryptodome: 3.18.0
        pygit2: Not Installed
  python-gnupg: Not Installed
        PyYAML: 6.0.1
         PyZMQ: 25.1.0
        relenv: Not Installed
         smmap: Not Installed
       timelib: Not Installed
       Tornado: 6.3.2
           ZMQ: 4.3.4
 
System Versions:
          dist: opensuse-tumbleweed 20230724 n/a
        locale: utf-8
       machine: x86_64
       release: 5.4.109+
        system: Linux
       version: openSUSE Tumbleweed 20230724 n/a
 
# salt-call --local grains.item init kernel kernelrelease kernelversion locale_info lsb_distrib_codename lsb_distrib_id lsb_distrib_release os os_family osarch osbuild oscodename osfinger osfullname osmajorrelease osmanufacturer osrelease osrelease_info osversion pythonexecutable pythonpath pythonversion saltpath saltversion saltversioninfo systemd virtual virtual_subtype zmqversion
local:
    ----------
    init:
        unknown
    kernel:
        Linux
    kernelrelease:
        5.4.109+
    kernelversion:
        #1 SMP Wed Jun 16 20:00:10 PDT 2021
    locale_info:
        ----------
        defaultencoding:
            UTF-8
        defaultlanguage:
            en_US
        detectedencoding:
            utf-8
        timezone:
            unknown
    lsb_distrib_codename:
        openSUSE Tumbleweed
    lsb_distrib_id:
        openSUSE Tumbleweed
    lsb_distrib_release:
        20230724
    os:
        SUSE
    os_family:
        Suse
    osarch:
        x86_64
    osbuild:
    oscodename:
        openSUSE Tumbleweed
    osfinger:
        openSUSE Tumbleweed-20230724
    osfullname:
        openSUSE Tumbleweed
    osmajorrelease:
        20230724
    osmanufacturer:
    osrelease:
        20230724
    osrelease_info:
        - 20230724
    osversion:
    pythonexecutable:
        /usr/bin/python3.11
    pythonpath:
        - /usr/bin
        - /usr/lib64/python311.zip
        - /usr/lib64/python3.11
        - /usr/lib64/python3.11/lib-dynload
        - /usr/lib64/python3.11/site-packages
        - /usr/lib64/python3.11/_import_failed
        - /usr/lib/python3.11/site-packages
    pythonversion:
        - 3
        - 11
        - 4
        - final
        - 0
    saltpath:
        /usr/lib/python3.11/site-packages/salt
    saltversion:
        3007.0+0na.0a98359
    saltversioninfo:
        - 3007
        - 0
    systemd:
        ----------
        features:
            +PAM +AUDIT +SELINUX +APPARMOR +IMA -SMACK +SECCOMP +GCRYPT +GNUTLS +OPENSSL +ACL +BLKID +CURL +ELFUTILS +FIDO2 +IDN2 -IDN +IPTC +KMOD +LIBCRYPTSETUP +LIBFDISK +PCRE2 +PWQUALITY +P11KIT +QRENCODE +TPM2 +BZIP2 +LZ4 +XZ +ZLIB +ZSTD +BPF_FRAMEWORK -XKBCOMMON +UTMP +SYSVINIT default-hierarchy=unified
        version:
            253
    virtual:
        container
    virtual_subtype:
        Docker
    zmqversion:
        4.3.4
# salt-call --local pkg.list_pkgs
local:
    ----------
    aaa_base:
        84.87+git20230329.b39efbc-1.2
    alts:
        1.2+30.a5431e9-1.2
    bash:
        5.2.15-8.4
    bash-sh:
        5.2.15-8.4
    binutils:
        2.40-5.2
    boost-license1_82_0:
        1.82.0-1.3
    ca-certificates:
        2+git20230406.2dae8b7-1.2
    ca-certificates-mozilla:
        2.60-3.1
    chkstat:
        1699_20230602-1.4
    compat-usrmerge-tools:
        84.87-5.14
    coreutils:
        9.3-1.2
    cpp:
        13-1.3
    cpp13:
        13.1.1+git7552-1.1
    cracklib-dict-small:
        2.9.11-1.1
    crypto-policies:
        20230420.3d08ae7-1.4
    curl:
        8.1.2-1.1
    dbus-1:
        1.14.8-1.2
    dbus-1-common:
        1.14.8-1.2
    dbus-1-daemon:
        1.14.8-1.2
    dbus-1-tools:
        1.14.8-1.2
    diffutils:
        3.10-1.2
    file:
        5.44-2.2
    file-magic:
        5.44-2.2
    filesystem:
        84.87-12.1
    fillup:
        1.42-279.3
    findutils:
        4.9.0-2.3
    gawk:
        5.2.2-2.2
    gcc:
        13-1.3
    gcc-c++:
        13-1.3
    gcc13:
        13.1.1+git7552-1.1
    gcc13-c++:
        13.1.1+git7552-1.1
    git:
        2.41.0-1.2
    git-core:
        2.41.0-1.2
    glibc:
        2.37-5.1
    glibc-devel:
        2.37-5.1
    glibc-locale:
        2.37-5.1
    glibc-locale-base:
        2.37-5.1
    gpg2:
        2.3.8-3.1
    grep:
        3.11-1.2
    gzip:
        1.12-3.3
    hostname:
        3.23-3.2
    kbd:
        2.6.1-1.1
    kbd-legacy:
        2.6.1-1.1
    kmod:
        30-6.1
    krb5:
        1.21.1-1.1
    less:
        633-2.2
    libabsl2301_0_0:
        20230125.3-2.1
    libacl1:
        2.3.1-2.9
    libalternatives1:
        1.2+30.a5431e9-1.2
    libapparmor1:
        3.1.6-1.1
    libargon2-1:
        20190702-2.6
    libasan8:
        13.1.1+git7552-1.1
    libassuan0:
        2.5.6-1.1
    libatomic1:
        13.1.1+git7552-1.1
    libattr1:
        2.5.1-1.25
    libaudit1:
        3.1.1-1.1
    libaugeas0:
        1.13.0-2.2
    libblkid1:
        2.39-3.1
    libboost_thread1_82_0:
        1.82.0-1.3
    libbrotlicommon1:
        1.0.9-3.4
    libbrotlidec1:
        1.0.9-3.4
    libbz2-1:
        1.0.8-5.5
    libcap-ng0:
        0.8.3-2.2
    libcap2:
        2.69-1.2
    libcbor0_10:
        0.10.2-1.2
    libcom_err2:
        1.47.0-1.2
    libcrypt1:
        4.4.35-1.1
    libcryptsetup12:
        2.6.1-3.1
    libctf-nobfd0:
        2.40-5.2
    libctf0:
        2.40-5.2
    libcurl4:
        8.1.2-1.1
    libdb-4_8:
        4.8.30-42.1
    libdbus-1-3:
        1.14.8-1.2
    libdevmapper1_03:
        2.03.16_1.02.185-7.1
    libeconf0:
        0.5.2-1.3
    libedit0:
        20210910.3.1-2.2
    libexpat1:
        2.5.0-2.3
    libfa1:
        1.13.0-2.2
    libfdisk1:
        2.39-3.1
    libffi8:
        3.4.4-1.3
    libfido2-1:
        1.13.0-1.3
    libgcc_s1:
        13.1.1+git7552-1.1
    libgcrypt20:
        1.10.2-2.2
    libgdbm6:
        1.23-1.12
    libgdbm_compat4:
        1.23-1.12
    libglib-2_0-0:
        2.76.4-1.1
    libgmp10:
        6.2.1-4.13
    libgomp1:
        13.1.1+git7552-1.1
    libgpg-error0:
        1.47-1.2
    libgpgme11:
        1.21.0-1.1
    libhidapi-hidraw0:
        0.13.1-1.3
    libhwasan0:
        13.1.1+git7552-1.1
    libidn2-0:
        2.3.4-1.3
    libip4tc2:
        1.8.9-1.3
    libisl23:
        0.26-1.2
    libitm1:
        13.1.1+git7552-1.1
    libjson-c5:
        0.16-1.6
    libkeyutils1:
        1.6.3-6.2
    libkmod2:
        30-6.1
    libksba8:
        1.6.4-1.1
    libldap2:
        2.6.4-2.1
    liblsan0:
        13.1.1+git7552-1.1
    liblua5_4-5:
        5.4.6-1.2
    liblz4-1:
        1.9.4-2.5
    liblzma5:
        5.4.3-1.2
    libmagic1:
        5.44-2.2
    libmount1:
        2.39-3.1
    libmpc3:
        1.3.1-1.3
    libmpdec3:
        2.5.1-2.14
    libmpfr6:
        4.2.0-3.1
    libncurses6:
        6.4.20230701-16.2
    libnghttp2-14:
        1.55.1-1.1
    libnpth0:
        1.6-2.11
    libnss_usrfiles2:
        2.27-3.13
    libopenssl1_1:
        1.1.1u-5.1
    libopenssl3:
        3.1.1-3.1
    libp11-kit0:
        0.24.1-2.3
    libpcre2-8-0:
        10.42-3.8
    libpgm-5_3-0:
        5.3.128-2.1
    libpkgconf3:
        1.8.0-2.3
    libpopt0:
        1.19-1.3
    libprocps8:
        3.3.17-12.3
    libprotobuf-lite23_4_0:
        23.4-6.1
    libproxy1:
        0.4.18-2.1
    libpsl5:
        0.21.2-1.3
    libpython2_7-1_0:
        2.7.18-36.4
    libpython3_11-1_0:
        3.11.4-1.2
    libreadline8:
        8.2-2.3
    libsasl2-3:
        2.1.28-5.2
    libseccomp2:
        2.5.4-2.5
    libselinux1:
        3.5-3.2
    libsemanage-conf:
        3.5-1.4
    libsemanage2:
        3.5-1.4
    libsepol2:
        3.5-1.3
    libsha1detectcoll1:
        1.0.3-4.19
    libsigc-2_0-0:
        2.12.0-1.2
    libsmartcols1:
        2.39-3.1
    libsodium-devel:
        1.0.18-2.14
    libsodium23:
        1.0.18-2.14
    libsolv-tools:
        0.7.24-1.3
    libsqlite3-0:
        3.42.0-1.2
    libssh-config:
        0.10.5-1.2
    libssh4:
        0.10.5-1.2
    libstdc++6:
        13.1.1+git7552-1.1
    libstdc++6-devel-gcc13:
        13.1.1+git7552-1.1
    libsubid4:
        4.13-6.2
    libsystemd0:
        253.7-1.1
    libtasn1-6:
        4.19.0-1.4
    libtsan2:
        13.1.1+git7552-1.1
    libubsan1:
        13.1.1+git7552-1.1
    libudev1:
        253.7-1.1
    libunistring5:
        1.1-2.2
    libunwind-coredump0:
        1.7.0-1.1
    libunwind-devel:
        1.7.0-1.1
    libunwind-ptrace0:
        1.7.0-1.1
    libunwind-setjmp0:
        1.7.0-1.1
    libunwind8:
        1.7.0-1.1
    libusb-1_0-0:
        1.0.26-1.6
    libutempter0:
        1.2.1-1.2
    libuuid1:
        2.39-3.1
    libverto1:
        0.3.2-3.1
    libwtmpdb0:
        0.7.1-1.1
    libxcrypt-devel:
        4.4.35-1.1
    libxml2-2:
        2.10.4-2.2
    libyaml-cpp0_7:
        0.7.0-2.2
    libz1:
        1.2.13-4.2
    libzck1:
        1.3.1-1.1
    libzmq5:
        4.3.4-4.3
    libzstd1:
        1.5.5-3.2
    libzypp:
        17.31.15-1.4
    linux-glibc-devel:
        6.4-1.1
    login_defs:
        4.13-6.2
    lsb-release:
        3.2-1.5
    ncurses-utils:
        6.4.20230701-16.2
    net-tools:
        2.10-2.2
    net-tools-deprecated:
        2.10-2.2
    netcfg:
        11.6-10.3
    openSUSE-build-key:
        1.0-48.1
    openSUSE-release:
        20230724-2409.1
    openSUSE-release-appliance-docker:
        20230724-2409.1
    openpgm-devel:
        5.3.128-2.1
    openssh:
        9.3p2-1.1
    openssh-clients:
        9.3p2-1.1
    openssh-common:
        9.3p2-1.1
    openssh-fips:
        9.3p2-1.1
    openssh-server:
        9.3p2-1.1
    openssl:
        3.1.1-2.1
    openssl-3:
        3.1.1-3.1
    p11-kit:
        0.24.1-2.3
    p11-kit-tools:
        0.24.1-2.3
    pam:
        1.5.3-1.3
    pam-config:
        2.5-1.2
    patterns-base-fips:
        20200505-41.1
    perl:
        5.36.1-1.2
    perl-Error:
        0.17029-1.16
    perl-Git:
        2.41.0-1.2
    perl-base:
        5.36.1-1.2
    permissions:
        1699_20230602-1.4
    permissions-config:
        1699_20230602-1.4
    pinentry:
        1.2.1-3.1
    pkgconf:
        1.8.0-2.3
    pkgconf-m4:
        1.8.0-2.3
    pkgconf-pkg-config:
        1.8.0-2.3
    procps:
        3.3.17-12.3
    python:
        2.7.18-36.1
    python-base:
        2.7.18-36.4
    python311-base:
        3.11.4-1.2
    python311-devel:
        3.11.4-1.2
    python311-pip:
        23.1.2-2.2
    python311-pyzmq:
        25.0.2-1.2
    python311-pyzmq-devel:
        25.0.2-1.2
    python311-setuptools:
        67.8.0-1.3
    rpm:
        4.18.0-5.2
    rpm-config-SUSE:
        20230712-1.1
    sed:
        4.9-2.3
    shadow:
        4.13-6.2
    sudo:
        1.9.13p3-3.4
    suse-module-tools:
        16.0.32-1.1
    system-group-hardware:
        20170617-24.13
    system-group-kvm:
        20170617-24.13
    system-user-root:
        20190513-2.11
    systemd:
        253.7-1.1
    systemd-default-settings:
        0.7-2.7
    systemd-default-settings-branding-SLE:
        0.7-2.7
    systemd-presets-branding-MicroOS:
        20230214-2.2
    systemd-presets-common-SUSE:
        15-30.1
    systemd-rpm-macros:
        24-1.1
    sysuser-shadow:
        3.1-6.1
    tar:
        1.34-11.1
    terminfo-base:
        6.4.20230701-16.2
    timezone:
        2023c-1.2
    udev:
        253.7-1.1
    update-alternatives:
        1.21.22-1.1
    util-linux:
        2.39-3.1
    which:
        2.21-5.9
    xz:
        5.4.3-1.2
    zeromq-devel:
        4.3.4-4.3
    zypper:
        1.14.61-1.1
# salt-call --local pip.list
local:
    ----------
    Jinja2:
        3.1.2
    MarkupSafe:
        2.1.3
    PyYAML:
        6.0.1
    certifi:
        2023.7.22
    cffi:
        1.15.1
    charset-normalizer:
        3.2.0
    contextvars:
        2.4
    cryptography:
        41.0.2
    distro:
        1.8.0
    idna:
        3.4
    immutables:
        0.19
    jmespath:
        1.0.1
    looseversion:
        1.3.0
    msgpack:
        1.0.5
    packaging:
        23.1
    pip:
        23.1.2
    psutil:
        5.9.5
    pycparser:
        2.21
    pycryptodomex:
        3.18.0
    pyzmq:
        25.1.0
    requests:
        2.31.0
    salt:
        3007.0+0na.0a98359
    setuptools:
        68.0.0
    tornado:
        6.3.2
    urllib3:
        2.0.4
    wheel:
        0.41.0
# salt-call --local locale.list_avail
local:
    - C
    - C.utf8
    - POSIX
    - aa_DJ
    - aa_DJ.utf8
    - aa_ER
    - aa_ER@saaho
    - aa_ET
    - af_ZA
    - af_ZA.utf8
    - agr_PE
    - ak_GH
    - am_ET
    - an_ES
    - an_ES.utf8
    - anp_IN
    - ar_AE
    - ar_AE.utf8
    - ar_BH
    - ar_BH.utf8
    - ar_DZ
    - ar_DZ.utf8
    - ar_EG
    - ar_EG.utf8
    - ar_IN
    - ar_IQ
    - ar_IQ.utf8
    - ar_JO
    - ar_JO.utf8
    - ar_KW
    - ar_KW.utf8
    - ar_LB
    - ar_LB.utf8
    - ar_LY
    - ar_LY.utf8
    - ar_MA
    - ar_MA.utf8
    - ar_OM
    - ar_OM.utf8
    - ar_QA
    - ar_QA.utf8
    - ar_SA
    - ar_SA.utf8
    - ar_SD
    - ar_SD.utf8
    - ar_SS
    - ar_SY
    - ar_SY.utf8
    - ar_TN
    - ar_TN.utf8
    - ar_YE
    - ar_YE.utf8
    - as_IN
    - ast_ES
    - ast_ES.utf8
    - ayc_PE
    - az_AZ
    - az_IR
    - be_BY
    - be_BY.utf8
    - be_BY@latin
    - bem_ZM
    - ber_DZ
    - ber_MA
    - bg_BG
    - bg_BG.utf8
    - bhb_IN.utf8
    - bho_IN
    - bho_NP
    - bi_VU
    - bn_BD
    - bn_IN
    - bo_CN
    - bo_IN
    - br_FR
    - br_FR.utf8
    - br_FR@euro
    - brx_IN
    - bs_BA
    - bs_BA.utf8
    - byn_ER
    - ca_AD
    - ca_AD.utf8
    - ca_ES
    - ca_ES.utf8
    - ca_ES@euro
    - ca_ES@valencia
    - ca_FR
    - ca_FR.utf8
    - ca_IT
    - ca_IT.utf8
    - ce_RU
    - chr_US
    - ckb_IQ
    - cmn_TW
    - crh_UA
    - cs_CZ
    - cs_CZ.utf8
    - csb_PL
    - cv_RU
    - cy_GB
    - cy_GB.utf8
    - da_DK
    - da_DK.utf8
    - de_AT
    - de_AT.utf8
    - de_AT@euro
    - de_BE
    - de_BE.utf8
    - de_BE@euro
    - de_CH
    - de_CH.utf8
    - de_DE
    - de_DE.utf8
    - de_DE@euro
    - de_IT
    - de_IT.utf8
    - de_LI.utf8
    - de_LU
    - de_LU.utf8
    - de_LU@euro
    - doi_IN
    - dsb_DE
    - dv_MV
    - dz_BT
    - el_CY
    - el_CY.utf8
    - el_GR
    - el_GR.utf8
    - el_GR@euro
    - en_AG
    - en_AU
    - en_AU.utf8
    - en_BW
    - en_BW.utf8
    - en_CA
    - en_CA.utf8
    - en_DK
    - en_DK.utf8
    - en_GB
    - en_GB.iso885915
    - en_GB.utf8
    - en_HK
    - en_HK.utf8
    - en_IE
    - en_IE.utf8
    - en_IE@euro
    - en_IL
    - en_IN
    - en_NG
    - en_NZ
    - en_NZ.utf8
    - en_PH
    - en_PH.utf8
    - en_SC.utf8
    - en_SG
    - en_SG.utf8
    - en_US
    - en_US.iso885915
    - en_US.utf8
    - en_ZA
    - en_ZA.utf8
    - en_ZM
    - en_ZW
    - en_ZW.utf8
    - eo
    - es_AR
    - es_AR.utf8
    - es_BO
    - es_BO.utf8
    - es_CL
    - es_CL.utf8
    - es_CO
    - es_CO.utf8
    - es_CR
    - es_CR.utf8
    - es_CU
    - es_DO
    - es_DO.utf8
    - es_EC
    - es_EC.utf8
    - es_ES
    - es_ES.utf8
    - es_ES@euro
    - es_GT
    - es_GT.utf8
    - es_HN
    - es_HN.utf8
    - es_MX
    - es_MX.utf8
    - es_NI
    - es_NI.utf8
    - es_PA
    - es_PA.utf8
    - es_PE
    - es_PE.utf8
    - es_PR
    - es_PR.utf8
    - es_PY
    - es_PY.utf8
    - es_SV
    - es_SV.utf8
    - es_US
    - es_US.utf8
    - es_UY
    - es_UY.utf8
    - es_VE
    - es_VE.utf8
    - et_EE
    - et_EE.iso885915
    - et_EE.utf8
    - eu_ES
    - eu_ES.utf8
    - eu_ES@euro
    - fa_IR
    - ff_SN
    - fi_FI
    - fi_FI.utf8
    - fi_FI@euro
    - fil_PH
    - fo_FO
    - fo_FO.utf8
    - fr_BE
    - fr_BE.utf8
    - fr_BE@euro
    - fr_CA
    - fr_CA.utf8
    - fr_CH
    - fr_CH.utf8
    - fr_FR
    - fr_FR.utf8
    - fr_FR@euro
    - fr_LU
    - fr_LU.utf8
    - fr_LU@euro
    - fur_IT
    - fy_DE
    - fy_NL
    - ga_IE
    - ga_IE.utf8
    - ga_IE@euro
    - gd_GB
    - gd_GB.utf8
    - gez_ER
    - gez_ER@abegede
    - gez_ET
    - gez_ET@abegede
    - gl_ES
    - gl_ES.utf8
    - gl_ES@euro
    - gu_IN
    - gv_GB
    - gv_GB.utf8
    - ha_NG
    - hak_TW
    - he_IL
    - he_IL.utf8
    - hi_IN
    - hif_FJ
    - hne_IN
    - hr_HR
    - hr_HR.utf8
    - hsb_DE
    - hsb_DE.utf8
    - ht_HT
    - hu_HU
    - hu_HU.utf8
    - hy_AM
    - hy_AM.armscii8
    - ia_FR
    - id_ID
    - id_ID.utf8
    - ig_NG
    - ik_CA
    - is_IS
    - is_IS.utf8
    - it_CH
    - it_CH.utf8
    - it_IT
    - it_IT.utf8
    - it_IT@euro
    - iu_CA
    - ja_JP.eucjp
    - ja_JP.shiftjisx0213
    - ja_JP.sjis
    - ja_JP.utf8
    - ka_GE
    - ka_GE.utf8
    - kab_DZ
    - kk_KZ
    - kk_KZ.utf8
    - kl_GL
    - kl_GL.utf8
    - km_KH
    - kn_IN
    - ko_KR.euckr
    - ko_KR.utf8
    - kok_IN
    - ks_IN
    - ks_IN@devanagari
    - ku_TR
    - ku_TR.utf8
    - kw_GB
    - kw_GB.utf8
    - ky_KG
    - lb_LU
    - lg_UG
    - lg_UG.utf8
    - li_BE
    - li_NL
    - lij_IT
    - ln_CD
    - lo_LA
    - lt_LT
    - lt_LT.utf8
    - lv_LV
    - lv_LV.utf8
    - lzh_TW
    - mag_IN
    - mai_IN
    - mai_NP
    - mfe_MU
    - mg_MG
    - mg_MG.utf8
    - mhr_RU
    - mi_NZ
    - mi_NZ.utf8
    - miq_NI
    - mjw_IN
    - mk_MK
    - mk_MK.utf8
    - ml_IN
    - mn_MN
    - mni_IN
    - mnw_MM
    - mr_IN
    - ms_MY
    - ms_MY.utf8
    - mt_MT
    - mt_MT.utf8
    - my_MM
    - nan_TW
    - nan_TW@latin
    - nb_NO
    - nb_NO.utf8
    - nds_DE
    - nds_NL
    - ne_NP
    - nhn_MX
    - niu_NU
    - niu_NZ
    - nl_AW
    - nl_BE
    - nl_BE.utf8
    - nl_BE@euro
    - nl_NL
    - nl_NL.utf8
    - nl_NL@euro
    - nn_NO
    - nn_NO.utf8
    - no_NO
    - no_NO.utf8
    - nr_ZA
    - nso_ZA
    - oc_FR
    - oc_FR.utf8
    - om_ET
    - om_KE
    - om_KE.utf8
    - or_IN
    - os_RU
    - pa_IN
    - pa_PK
    - pap_AW
    - pap_CW
    - pl_PL
    - pl_PL.utf8
    - ps_AF
    - pt_BR
    - pt_BR.utf8
    - pt_PT
    - pt_PT.utf8
    - pt_PT@euro
    - quz_PE
    - raj_IN
    - rif_MA
    - ro_RO
    - ro_RO.utf8
    - ru_RU
    - ru_RU.koi8r
    - ru_RU.utf8
    - ru_UA
    - ru_UA.utf8
    - rw_RW
    - sa_IN
    - sah_RU
    - sat_IN
    - sc_IT
    - sd_IN
    - sd_IN@devanagari
    - se_NO
    - sgs_LT
    - shn_MM
    - shs_CA
    - si_LK
    - sid_ET
    - sk_SK
    - sk_SK.utf8
    - sl_SI
    - sl_SI.utf8
    - sm_WS
    - so_DJ
    - so_DJ.utf8
    - so_ET
    - so_KE
    - so_KE.utf8
    - so_SO
    - so_SO.utf8
    - sq_AL
    - sq_AL.utf8
    - sq_MK
    - sr_ME
    - sr_RS
    - sr_RS@latin
    - ss_ZA
    - st_ZA
    - st_ZA.utf8
    - sv_FI
    - sv_FI.utf8
    - sv_FI@euro
    - sv_SE
    - sv_SE.utf8
    - sw_KE
    - sw_TZ
    - syr
    - szl_PL
    - ta_IN
    - ta_LK
    - tcy_IN.utf8
    - te_IN
    - tg_TJ
    - tg_TJ.utf8
    - th_TH
    - th_TH.utf8
    - the_NP
    - ti_ER
    - ti_ET
    - tig_ER
    - tk_TM
    - tl_PH
    - tl_PH.utf8
    - tn_ZA
    - to_TO
    - tpi_PG
    - tr_CY
    - tr_CY.utf8
    - tr_TR
    - tr_TR.utf8
    - ts_ZA
    - tt_RU
    - tt_RU@iqtelif
    - ug_CN
    - uk_UA
    - uk_UA.utf8
    - unm_US
    - ur_IN
    - ur_PK
    - uz_UZ
    - uz_UZ.utf8
    - uz_UZ@cyrillic
    - ve_ZA
    - vi_VN
    - wa_BE
    - wa_BE.utf8
    - wa_BE@euro
    - wae_CH
    - wal_ET
    - wo_SN
    - xh_ZA
    - xh_ZA.utf8
    - yi_US
    - yi_US.utf8
    - yo_NG
    - yue_HK
    - yuw_PG
    - zh_CN
    - zh_CN.gb18030
    - zh_CN.gbk
    - zh_CN.utf8
    - zh_HK
    - zh_HK.utf8
    - zh_SG
    - zh_SG.gbk
    - zh_SG.utf8
    - zh_TW
    - zh_TW.euctw
    - zh_TW.utf8
    - zu_ZA
    - zu_ZA.utf8
